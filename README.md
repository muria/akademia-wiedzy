# Akademia Wiedzy

This is source code for Akademia Wiedzy website.

Currently, this version is not yet used. The old implementation is available under [www.akademiawiedzy.pl](https://www.akademiawiedzy.pl/).

This source code does not include many of static files such as images and fonts.

![Akademia Wiedzy Screencap 1](screencaps/akademia_cap_1_90.webp)

![Akademia Wiedzy Screencap 2](screencaps/akademia_cap_2_90.webp)
