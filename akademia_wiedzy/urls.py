"""akademia_wiedzy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('tinymce/', include('tinymce.urls')),
    *static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT),
    path('', include('akademia.urls'))
]

handler404 = 'akademia.views.error_404'

admin.site.site_header = "Akademia Wiedzy – Panel Administracyjny"
admin.site.site_title = "Akademia Wiedzy"
admin.site.index_title = "Zarządzanie stroną"
