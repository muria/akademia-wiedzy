from django.contrib import admin

from akademia.models import Category, Article, Gallery, GalleryImage


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'category')


class ImageInline(admin.StackedInline):
    model = GalleryImage
    extra = 1


class GalleryAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'category']})
    ]
    inlines = [ImageInline]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            kwargs['queryset'] = Category.objects.filter(view_type='gallery')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Category)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Gallery, GalleryAdmin)
