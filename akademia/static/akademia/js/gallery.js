let galleries = [];

class Gallery {
    currentIndex = 0;

    constructor(gallery, fullscreenGallery) {
        this.galleryElement = fullscreenGallery;
        this.closedImages = gallery.querySelectorAll('.gallery_image .image');
        this.presentationImages = fullscreenGallery.querySelectorAll('.images .image');
        this.setCurrentIndex(0);
        this.galleryElement.addEventListener('click', this.backgroundClicked.bind(this));
        this.galleryElement.querySelector('.arrow-left').addEventListener('click', this.previusImage.bind(this));
        this.galleryElement.querySelector('.arrow-right').addEventListener('click', this.nextImage.bind(this));
        for (const image of this.closedImages) {
            image.addEventListener('click', this.openGallery.bind(this));
        }
    }

    previusImage(event) {
        if (this.currentIndex > 0) {
            this.setCurrentIndex(this.currentIndex - 1);
        }
    }

    nextImage(event) {
        if (this.currentIndex < this.presentationImages.length - 1) {
            this.setCurrentIndex(this.currentIndex + 1);
        }
    }

    setCurrentIndex(index) {
        this.currentIndex = index;
        for (var i=0; i < this.presentationImages.length; i++) {
            this.presentationImages[i].className = 'image';
            if (i == index - 1) {
                this.presentationImages[i].classList.add('previous');
            }
            else if (i == index) {
                this.presentationImages[i].classList.add('current');
            }
            else if (i == index + 1) {
                this.presentationImages[i].classList.add('next');
            }
        }
    }

    backgroundClicked(event) {
        if (event.target == this.galleryElement) {
            this.galleryElement.classList.remove('active');
        }
    }

    openGallery(event) {
        const galleryImage = event.target.parentNode;
        var index = Array.from(galleryImage.parentNode.children).indexOf(galleryImage);
        this.galleryElement.classList.add('active');
        this.setCurrentIndex(index);
    }
}

export function initGallery(gallery, fullscreenGallery) {
    galleries.push(new Gallery(gallery, fullscreenGallery));
}
