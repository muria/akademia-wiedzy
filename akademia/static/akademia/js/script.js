import { initGallery } from './gallery.js';

function parentOfClass(element, className) {
    let el = element;
    while (el && !el.classList.contains(className)) {
        el = el.parentElement;
    }
    return el;
}

function elementOuterHeight(element) {
    let height = element.clientHeight;
    const computedStyle = window.getComputedStyle(element);
    if (computedStyle.marginTop) {
        height += parseInt(computedStyle.marginTop);
    }
    if (computedStyle.marginBottom) {
        height += parseInt(computedStyle.marginBottom);
    }
    return height;
}

function tabButtonClicked(event) {
    const button = event.target;

    const buttons = button.parentElement.querySelectorAll('.tab_button');
    for (const tabButton of buttons) {
        if (tabButton === button) {
            tabButton.classList.add('active')
        }
        else {
            tabButton.classList.remove('active');
        }
    }

    const entries = button.parentElement.parentElement.querySelectorAll('.entries');
    for (const entry of entries) {
        if (entry.dataset.tabId && entry.dataset.tabId == button.dataset.tabName) {
            entry.classList.add('active');
        }
        else {
            entry.classList.remove('active');
        }
    }
}

function listEntryClicked(event) {
    const entry = parentOfClass(event.target, 'entry');
    const entries = parentOfClass(entry, 'entries');
    for (const listEntry of entries.querySelectorAll('.entry')) {
        let textWrapper = listEntry.querySelector('.text_wrapper');
        listEntry.classList.remove('active');
        textWrapper.style.height = 0;
    }
    entry.classList.add('active');
    const text = entry.querySelector('.text');
    let height = elementOuterHeight(text);
    entry.querySelector('.text_wrapper').style.height = height + 'px';
}

document.addEventListener("DOMContentLoaded", function(){
    const tabButtons = document.querySelectorAll('.tab_button');
    for (const button of tabButtons) {
        button.addEventListener("click", tabButtonClicked);
    }
    const listElements = document.querySelectorAll('.entries.list .entry');
    for (const element of listElements) {
        element.addEventListener('click', listEntryClicked);
    }

    const entriesGalleries = document.querySelectorAll('.entries.gallery');
    const fullscreenGalleriesList = document.querySelectorAll('.fullscreen_gallery');
    for (var i = 0; i < entriesGalleries.length; i++) {
        initGallery(entriesGalleries[i], fullscreenGalleriesList[i]);
    }
});
