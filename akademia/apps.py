from django.apps import AppConfig


class AkademiaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'akademia'
