from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template import loader

from akademia.models import Category, Article
from akademia.view_helpers import CategoryView


def index(request):
    index_category = Category.objects.get(index=True)
    return _category(request, index_category)


def category(request, category_name: str):
    current_category = get_object_or_404(Category, name=category_name)
    return _category(request, current_category)


def _category(request, current_category: Category):
    menu_items = Category.get_top_categories()

    template = loader.get_template('akademia/category.html')
    context = {
        'menu': [CategoryView(c) for c in menu_items],
        'category': CategoryView(current_category)
    }
    return HttpResponse(template.render(context, request))


def article(request, category_name: str, article_name: str):
    menu_items = Category.get_top_categories()
    article_object = get_object_or_404(Article, slug_name=article_name)
    navigation = article_object.category.get_navigation()
    top_category = navigation[0]
    template = loader.get_template('akademia/article.html')
    context = {
        'menu': menu_items,
        'navigation': navigation,
        'top_category': top_category,
        'article': article_object
    }
    return HttpResponse(template.render(context, request))


def error_404(request, exception):
    context = {
        'menu': Category.get_top_categories(),
        'error': '404',
        'text': str(exception)
    }
    template = loader.get_template('akademia/error.html')
    return HttpResponse(template.render(context, request))
