from __future__ import annotations
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import Type, Dict

from django.db import models
from django.db.models import QuerySet
from django.template.defaultfilters import slugify
from tinymce.models import HTMLField

from akademia.HTMLParser import RichTextParser


def unique_slug_name(element: models.Model, class_type: Type[models.Model], attribute: str, base: str) -> str:
    existing = {getattr(obj, attribute) for obj in class_type.objects.all() if obj is not element}
    slug_name = slugify(base)
    suffix: str = ''
    i: int = 1
    while slug_name + suffix in existing:
        suffix = '-' + str(i)
        i += 1
    return slug_name + suffix


class ViewTypes:
    class Text(Enum):
        NONE = 'none'
        EXCERPT = 'excerpt'
        FULL = 'full'

    class Orientation(Enum):
        HORIZONTAL = 'horizontal'
        VERTICAL = 'vertical'

    @dataclass
    class ViewTypeData:
        label: str
        show_text: ViewTypes.Text
        columns: bool
        orientation: ViewTypes.Orientation
        show_more: bool = False
        show_image_as_cover: bool = False
        show_title: bool = True
        title_link: bool = True
        extra_class: str = ''

    data: Dict[str, ViewTypeData] = {
        'img_text_horizontal': ViewTypeData('Miniatura + Teskt', Text.EXCERPT, False, Orientation.HORIZONTAL, True),
        'img_title_vertical': ViewTypeData('Miniatura + Tutuł, Pionowo', Text.NONE, True, Orientation.VERTICAL, False, True),
        'img_title_horizontal': ViewTypeData('Miniatura + Tutuł, Poziomo', Text.NONE, True, Orientation.HORIZONTAL, True, True),
        'circle_title_centered': ViewTypeData('Okrągła miniatura + Tytuł, Wyśrodkowane', Text.NONE, True, Orientation.VERTICAL, False, True, extra_class='circle'),
        'whole_no_title_img_right': ViewTypeData('Cały Wpis, Grafika po prawej', Text.FULL, False, Orientation.HORIZONTAL, False, False, False, True, 'full'),
        'list': ViewTypeData('Rozwijana lista', Text.FULL, False, Orientation.HORIZONTAL, False, title_link=False, extra_class='list'),
        'gallery': ViewTypeData('Galeria', Text.NONE, True, Orientation.HORIZONTAL, False, True, extra_class='gallery',),
        'hyperlink': ViewTypeData('Hyperlink', Text.NONE, False, Orientation.HORIZONTAL),
        'custom': ViewTypeData('Z szablonu HTML', Text.NONE, False, Orientation.HORIZONTAL)
    }

    @classmethod
    def choices(cls):
        return [(key, value.label) for key, value in cls.data.items()]

    @classmethod
    def default(cls):
        return next(iter(cls.data.keys()))

    @classmethod
    def show_text(cls, value: str) -> str:
        return cls.data[value].show_text.value

    @classmethod
    def columns(cls, value: str) -> bool:
        return cls.data[value].columns

    @classmethod
    def orientation(cls, value: str) -> str:
        return cls.data[value].orientation.value

    @classmethod
    def show_more(cls, value: str) -> bool:
        return cls.data[value].show_more

    @classmethod
    def show_image_as_cover(cls, value: str) -> bool:
        return cls.data[value].show_image_as_cover

    @classmethod
    def show_title(cls, value: str) -> bool:
        return cls.data[value].show_title

    @classmethod
    def title_link(cls, value: str) -> bool:
        return cls.data[value].title_link

    @classmethod
    def extra_class(cls, value: str) -> str:
        return cls.data[value].extra_class


class Category(models.Model):
    class Meta:
        ordering = ["order"]
        verbose_name_plural = "Kategorie"

    name: str = models.CharField('Nazwa porządkowa', max_length=70, editable=False, unique=True)
    label: str = models.CharField('Nazwa', max_length=64)
    parent = models.ForeignKey('self', verbose_name="Kategoria nadrzędna", blank=True, null=True, on_delete=models.CASCADE)
    view_type: str = models.CharField('Typ widoku', choices=ViewTypes.choices(), default=ViewTypes.default(), max_length=32)
    extra: str = models.CharField('Dane specjalne', max_length=512, blank=True)
    order = models.IntegerField(default=0)
    index = models.BooleanField("Domyślna na stronie głównej.", default=False, help_text="Włączenie tej opcji spowoduje wyświetlenie katerogii na stronie głównej.")

    def __str__(self):
        indent = '>' if self.parent else ''
        return indent + self.label

    def save(self, *args, **kwargs):
        self.name = unique_slug_name(self, Category, 'name', self.label)
        if self.index:
            all_categories = Category.objects.all()
            for category in all_categories:
                category.index = False
                category.save()
        if self.view_type == 'gallery':
            # Clear Articles for this category
            articles = Article.objects.filter(category=self)
            for article in articles:
                article.category = None
                article.save()
        else:
            # Clear Galleries for this category
            galleries = Gallery.objects.filter(category=self)
            for gallery in galleries:
                gallery.category = None
                gallery.save()
        super().save(*args, **kwargs)

    def get_navigation(self):
        navigation = [self]
        category = self
        while category.parent:
            category = category.parent
            navigation.insert(0, category)
        return navigation

    def has_any_entries(self):
        if self.articles() or self.galleries():
            return True
        else:
            return any(c.has_any_entries() for c in self.subcategories)

    @property
    def subcategories(self):
        return Category.objects.filter(parent=self)

    def articles(self):
        return Article.objects.filter(category=self)

    def galleries(self):
        return Gallery.objects.filter(category=self)

    @classmethod
    def get_top_categories(cls):
        return cls.objects.filter(parent=None)


class Article(models.Model):
    class Meta:
        verbose_name_plural = "Treści"
    title: str = models.CharField("Tytuł", max_length=255)
    slug_name: str = models.CharField(max_length=260, editable=False, unique=True)
    published = models.DateTimeField('Data publikacji', blank=True, null=True)
    category = models.ForeignKey(Category, verbose_name="Kategoria", null=True, blank=True, on_delete=models.CASCADE)
    image = models.ImageField("Obraz", upload_to="upload", null=True, blank=True)
    content: str = HTMLField("Treść", blank=True)

    def __str__(self):
        return self.title

    @property
    def formatted_content(self):
        parser = RichTextParser(self.content)
        return str(parser)

    def save(self, *args, **kwargs):
        if not self.published:
            self.published = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.slug_name = unique_slug_name(self, Article, 'slug_name', self.title)
        super().save(*args, **kwargs)

    @property
    def excerpt(self):
        parser = RichTextParser(self.content)
        parser.short(180)
        return str(parser)


class Gallery(models.Model):
    class Meta:
        verbose_name_plural = "Galerie"
    title: str = models.CharField('Tytuł', max_length=255)
    name: str = models.CharField(max_length=260, editable=False, unique=True)
    category = models.ForeignKey(Category, verbose_name="Kategoria", blank=True, null=True, on_delete=models.SET_NULL,
                                 help_text="Kategoria musi mieć ustawiony widok galerii aby móc ją wybrać.")

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.name = unique_slug_name(self, Article, 'slug_name', self.title)
        super().save(*args, **kwargs)

    @property
    def images(self) -> QuerySet[GalleryImage]:
        return GalleryImage.objects.filter(gallery=self)


class GalleryImage(models.Model):
    url = models.ImageField("Obraz", upload_to="upload", null=True, blank=True)
    description = models.TextField('Opis', blank=True)
    gallery = models.ForeignKey(Gallery, on_delete=models.CASCADE)
